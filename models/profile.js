const mongoose=require('mongoose');
const ProfileSchema=new mongoose.Schema({
    User:{type:mongoose.Schema.Types.ObjectId,ref:'User'},
    Address:{type:String},
    Phone:{type:String},
    DOB:{type:Date},
  status: {
    type: String
  },
  social: 
      {
    youtube: {
      type: String
    },
    twitter: {
      type: String
    },
    facebook: {
      type: String
    }
  },
  date:{type:Date,default:Date.now}
})
module.exports=mongoose.model('Profile',ProfileSchema);