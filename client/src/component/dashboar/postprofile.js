import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import moment from "moment";
import ReactMoment from "react-moment";
import { listpost, likeandunlike, deletepost } from "../../actions/postAction";
import { Link } from "react-router-dom";
import "./rightprofile.scss";
import ListComment from "../post/Listcomment";

import Comment from './Comment';
const PostProfile = props => {
  // const [show, setShow] = useState(true);
  // const visibility = show ? 'visible' : 'hidden';

  const { post } = props.post;

  const likeandunlike = id => {
    props.likeandunlike(id);
  };
  return (
    <div className="row">
      {post && post.length
        ? post.map((post, index) => (
            <div key={index} className="PostProfile">
              {(post.User.Image && post.User.Image.length) > 0 ? (
                <img
                  src={`../upload/user/${
                    post.User.Image[post.User.Image.length - 1]
                  }`}
                  className="postimageIcon"
                />
              ) : (
                <img
                  src={"../upload/noimage/noimage.jpg"}
                  className="postimageIcon"
                />
              )}
              <div className="namepost">{post.User.fullName}</div>
              <div className="time">
                {<ReactMoment fromNow>{post.Date}</ReactMoment>}
              </div>

              <div className="postprofiledetail">
                <p>{post.Detail}</p>
              </div>

              <div className="imgpostprofile">
                {(post.Image && post.Image.length) > 0 ? (
                  <img
                    src={`../upload/post/${post.Image[post.Image.length - 1]}`}
                  />
                ) : (
                  ""
                )}
              </div>

              <div className="menu">
                <button className="button">
                  <i className="fas fa-thumbs-up" /> Thích ({post.Like.length})
                </button>
                <button className="button">
                  <i className="far fa-comment-alt" /> Bình luận (
                  {post.Comment.length})
                </button>
                <button className="button">
                  <i className="fas fa-share-alt" /> Share
                </button>
              </div>
              <div>
              <div className='comment'>
                     <Comment comment={post.Comment}/>

              </div>
              {/* <h1 style={{ visibility }}>useState Hook is awesome!</h1>
      <button
        onClick={() => { setShow(!show) }}
      >Button</button> */}

              </div>
            </div>
          ))
        : ""}
    </div>
  );
};
const mapStateToProps = state => {
  return {
    user: state.user,
    post: state.post
  };
};
export default connect(
  mapStateToProps,
  { deletepost, likeandunlike }
)(PostProfile);
