import React, { useEffect } from "react";
import { connect } from "react-redux";
import { previewUser } from "../../actions/profileAction";
import { getpostprofile } from "../../actions/postAction";
import Postprofile from './postprofile';
import "./leftprofile.scss";
const DashBoar = props => {
  const { profile } = props.profile;
  useEffect(() => {
    props.previewUser(props.match.params.id);
  }, [props.previewUser]);

  
  useEffect(() => {
    if (props.match.params.id) {
      props.getpostprofile(props.match.params.id);
    }
  }, [props.getpostprofile]);
  
  return (
    <div className='row'>
      <div className='profile'>
        <img   src={"../upload/avatar/defaultBcg.jpeg"}/>
        <div className='row'>
             {(profile.Image && profile.Image.length) > 0 ? (
             <img
               src={`../upload/user/${profile.Image [profile.Image.length - 1]}`}
               width="200px"
               height="200px" className='imgprofile'
             />
           ) : (
             <img
               src={"../upload/noimage/noimage.jpg"}
               width="200px"
               height="200px" className='imgprofile'
             />
           )}
         </div>
         <div className='row'>
           <div className='col sm-4'>
           <div className='profileleft'> 
           <div className='row'>
            <div className="profilefullName">{profile.fullName}</div>
         </div>
         <div className='row'>
         <div className="emailprofile">{profile.email}</div>
         </div>
         <div className='row'>
        <div className='DetailStatus'>
            Status:{profile.Status ?profile.Status:'Không có '}
        </div>
      </div>
      <div className='row'>
        <div className='DetailAdress'>
            Adress:{profile.Address ?profile.Address:'Không có thông tin'}
        </div>
      </div>
      <div className='row'>
        <div className='DetailPhone'>
            Phone:{profile.Phone ?profile.Phone:'Không có thông tin'}
        </div>
      </div>

      <div className='row'>
        <div className='DetailPhone'>
          Hình Ảnh
        </div>
      </div>
      <div className='row'>
      <div className='imgDetail'>
           
           {(profile.Image && profile.Image.length) > 0 ? (
            profile.Image.splice(0,3).map((img,index)=>(
            
              <img key={index} src={`../upload/user/${img}`}/>
            
            ))
           ) :''}
          
      </div>
          
         </div>
           </div>
         
           </div>
           <div className='col sm-8'>
           <div className='profilerightname'>
           <Postprofile profile={props.post.post}/>
           </div>
         
           </div>
         </div>
        
      </div>
     
    </div>
    
  );
};
const mapStateToProps = state => {
  return {
    user: state.user,
    profile: state.profile,
    post: state.post
  };
};
export default connect(
  mapStateToProps,
  { previewUser, getpostprofile }
)(DashBoar);
