import React from 'react';
import {Link} from 'react-router-dom';

import './rightprofile.scss';
const Comment=(props)=>{
    const comment=props.comment;
    
    return (
        <div >
            {(comment && comment.length)>0 ?comment.map((comment,index)=>(
                <div key={index} className='commentprofile'>
                {(comment.User.Image && comment.User.Image.length) > 0 ? (
                <img
                  src={`../upload/user/${
                    comment.User.Image[comment.User.Image.length - 1]
                  }`}
                  className="commentimageIcon"
                />
              ) : (
                <img
                  src={"../upload/noimage/noimage.jpg"}
                  className="commentimageIcon"
                />
              )}
              <div className="nameprofile"></div>
             
              <div className='Commentprofile'>
              <p>   <Link to={"/Dashboar/" + comment.User._id}>{comment.User.fullName}{comment.User.id}</Link>:{comment.Comment}</p>
              </div>
                   
                </div>
            )):'' }
        </div>
    )
}
export default Comment;