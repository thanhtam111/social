import React, { useState,useEffect } from 'react'
import { Button, Form,  Input, FormFeedback } from 'reactstrap';

import { connect } from "react-redux";
import { commentpost } from "../../actions/postAction";
import 'antd/dist/antd.css';
import { Avatar } from 'antd';
import './postitem.css'
function PostComment(props){
  const [Comment,setComment]=useState('');
  const [errors,seterrors]=useState({});
  const id=props.user.user.id;
const onSubmit=e=>{
  e.preventDefault();
  const newComment=({
    Comment:Comment,
    User:id
  })
props.commentpost(props.id,newComment)  

}

useEffect(
  ()=>{
    seterrors(props.errors)
},[errors,props.errors])
    return (
        <div className='postcomment'>
        <div className='row'>
        
          <div className='left-postcomment'>  {props.user.user.Image?
          <Avatar src={`../upload/user/${props.user.user.Image}`} />:
          <Avatar src='../upload/noimage/noimage.jpg'  />
        }
        ({props.user.user.fullName})</div>
          <div className='right-postcomment'>
          <Form onSubmit={onSubmit}>
          <Input type='textarea' 
          nam='Comment'
          id='Comment'
          invalid={errors.Comment ?true:false}
          onChange={e=>setComment(e.target.value)}>

          </Input>
          <FormFeedback>{errors.Comment ? errors.Comment : null}</FormFeedback>
          <br/>
          <div >
        <Button color="info" >Post</Button>
        </div>
                  </Form>
          </div>
        </div>
        
      
        </div>
    )
}
const mapStateToProps = state => {
    return {
      post: state.post,
      user: state.user,
      errors:state.errors
    };
  };
export default connect(mapStateToProps,{commentpost})(PostComment)

