import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { geteditpost, postedit } from "../../actions/postAction";
import _ from "lodash";
import './commentpost.scss'
import { Button, Form, Input, FormFeedback } from "reactstrap";
const Editpost = props => {
  
  const [Detail, setDetail] = useState("");
  const [file, setfile] = useState([]);
  const [Image, setImage] = useState([]);
  const [errors, seterrors] = useState({});
  const id = props.user.user.id;
  const post = props.post.posts;
console.log(props.match.params.id)
  useEffect(() => {
    if (!_.isEmpty(props.post.posts)) {
      setDetail(props.post.posts.Detail);
      setImage(props.post.posts.Image);
    }
    
  }, [props.post.posts]);

  const onSubmit = e => {
    e.preventDefault();
    const formData = new FormData();
    for (let i = 0; i < file.length; i++) {
      formData.append(Image, file[i]);
    }
    formData.append("Detail", Detail);
    formData.append("User", id);
    props.postedit(props.match.params.id, formData,props.history);
  };
  useEffect(() => {
    seterrors(props.errors);
  }, [errors, props.errors]);
  const deleteid = id => {
    if (window.confirm("Are you sure")) {
      const a = [...file].filter(e => e != id);
      setfile(a);
    }
  };

  useEffect(() => {
    props.geteditpost(props.match.params.id);
  }, [props.geteditpost]);

const image=(Image && Image.length)>0? Image.map((img,index)=>(
<img src={`../upload/post/${img}`} key={index} width='50px'></img>
)):''
  return (
    <div className='row'>
       <div className="editpost">
      <h3>Edit Post</h3>
      <div className="formeditpost">
        <Form onSubmit={onSubmit}>
         <div className='row'>
           <div className='col-sm-4'>
             <div className='Detail'>
             <h4>Detail</h4>
             </div>
           </div>
           <div className='col-sm-8'>
           <Input
              type="textarea"
              name="Detail"
              id="Detail"
              value={Detail}
              onChange={e => setDetail(e.target.value)}
              invalid={errors.Detail ? true : false}
            />
            <FormFeedback>{errors.Detail ? errors.Detail : null}</FormFeedback>
           </div>
         </div>
         <br/>
         <div className='row'>
           <div className='col-sm-4'>
             <div className='Detail'>
             <h4>Image</h4>
             </div>
           </div>
           <div className='col-sm-8'>
           <Input
              type="file"
              name="file"
              id="file"
              multiple
              placeholder="Enter file..."
              onChange={e => setfile(e.target.files)}
              accept="image/*"
            />

            {(file && file.length) > 0
              ? [...file].map((imgs, index) => (
                  <div key={index} className="imageproduct">
                  <div className='image'>
                  <img src={URL.createObjectURL(imgs)} />
                  </div>
                    <div className='span'>
                    <span onClick={deleteid.bind(this, imgs)} className='span' >
                      <i className="fa fa-window-close" />
                    </span>
                    </div>

                  
                  </div>
                ))
              : ""}
           </div>
         </div>
          <br />
        

                <div className='row'>
                <div className="button-post">
            <Button color="info">
              Edit
            </Button>
          </div>
                </div>
          
        </Form>
      </div>
    </div>
    </div>
   
  );
};
const mapStateToProps = state => {
  return {
    user: state.user,
    errors: state.errors,
    post: state.post
  };
};
export default connect(
  mapStateToProps,
  { geteditpost, postedit }
)(Editpost);
