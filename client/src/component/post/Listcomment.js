import React, { useEffect, useState } from "react";

import moment from "moment";
import ReactMoment from "react-moment";
import {Link} from 'react-router-dom';
import "./post.scss";

const PostItem = props => {
  const [visiable,setvisiable]=useState(3);
  const Comment = props.comment;
const load=()=>{
setvisiable({visiable:visiable+3})
  
}
  return (
    <div className="row">
      <div className="Comment">
        {(Comment && Comment.length) > 0
          ? Comment
          
          .map((comment, index) => (
            <div key={index} className='commentprofile'>
                {(comment.User.Image && comment.User.Image.length) > 0 ? (
                <img
                  src={`../upload/user/${
                    comment.User.Image[comment.User.Image.length - 1]
                  }`}
                  className="commentimageIcon"
                />
              ) : (
                <img
                  src={"../upload/noimage/noimage.jpg"}
                  className="commentimageIcon"
                />
              )}
              <div className="nameprofile"></div>
             
              <div className='Commentprofile'>
              <p>   <Link to={"/Dashboar/" + comment.User._id}>{comment.User.fullName}{comment.User.id}</Link>:{comment.Comment}</p>
              </div>
       
                </div>
            ))
          : ""}
          {/* {Comment.length?  <div className='hienthi'>
       <button onClick={load.bind()}>Hienthithem></button>
         </div>  :''} */}
            
      </div>
    
    </div>
  );
};

export default PostItem;
