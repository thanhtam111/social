import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { geteditpost, postedit } from "../../../actions/postAction";
import _ from "lodash";
import "./post.css";
import { Button, Form, Input, FormFeedback } from "reactstrap";
const Newpost = props => {
  // const [Title, setTitle] = useState("");
  const [Detail, setDetail] = useState("");
  const [file, setfile] = useState([]);
  const [Image, setImage] = useState([]);
  const [errors, seterrors] = useState({});
  const id = props.user.user.id;
  const post = props.post.post;

  useEffect(() => {
    if (!_.isEmpty(props.post.post)) {
      // setTitle(props.post.post.Title);
      setDetail(props.post.post.Detail);
      setImage(props.post.post.Image);
    }
    
  }, [props.post.post]);

  const onSubmit = e => {
    e.preventDefault();
    const formData = new FormData();
    for (let i = 0; i < file.length; i++) {
      formData.append(Image, file[i]);
    }
    // formData.append("Title", Title);
    formData.append("Detail", Detail);
    formData.append("User", id);
    console.log(formData)
    props.postedit(props.match.params.id, formData,props.history);
  };
  useEffect(() => {
    seterrors(props.errors);
  }, [errors, props.errors]);
  const deleteid = id => {
    if (window.confirm("Are you sure")) {
      const a = [...file].filter(e => e != id);
      setfile(a);
    }
  };

  useEffect(() => {
    props.geteditpost(props.match.params.id);
  }, [props.geteditpost]);

const image=(Image && Image.length)>0? Image.map((img,index)=>(
<img src={`../upload/post/${img}`} key={index} width='50px'></img>
)):''
  return (
    <div className="createpost">
      <h3>Create Post</h3>
      <div className="formcreatepost">
        <Form onSubmit={onSubmit}>
          {/* <div>
            <h4>Title</h4>
            <Input
              type="text"
              name="Title"
              id="Title"
              value={Title || ""}
              onChange={e => setTitle(e.target.value)}
              invalid={errors.Title ? true : false}
            />
            <FormFeedback>{errors.Title ? errors.Title : null}</FormFeedback>
          </div> */}
          <br />
          <div>
            <h4>Detail</h4>
            <Input
              type="textarea"
              name="Detail"
              id="Detail"
              value={Detail}
              onChange={e => setDetail(e.target.value)}
              invalid={errors.Detail ? true : false}
            />
            <FormFeedback>{errors.Detail ? errors.Detail : null}</FormFeedback>
          </div>
          <br />

          <div className="input-group-prepend">
            <h3>Image:</h3>
          </div>
          <div>
            <Input
              type="file"
              name="file"
              id="file"
              multiple
              placeholder="Enter file..."
              onChange={e => setfile(e.target.files)}
              accept="image/*"
            />

            {(file && file.length) > 0
              ? [...file].map((imgs, index) => (
                  <div key={index} className="imageproduct">
                    <img src={URL.createObjectURL(imgs)} />

                    <span onClick={deleteid.bind(this, imgs)}>
                      <i className="fa fa-window-close" />
                    </span>
                  </div>
                ))
              : ""}
          </div>
          <br />
                  <div>
                    {image}
                  </div>
          <div className="button-post">
            <Button color="info" size="lg">
              Post
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
};
const mapStateToProps = state => {
  return {
    user: state.user,
    errors: state.errors,
    post: state.post
  };
};
export default connect(
  mapStateToProps,
  { geteditpost, postedit }
)(Newpost);
