import React, { useEffect, useState } from "react";
import moment from "moment";
import ReactMoment from "react-moment";
import { connect } from "react-redux";
import { listpost, likeandunlike, deletepost } from "../../actions/postAction";
import { Link } from "react-router-dom";
import PostComment from "./newcoment";
import ListComment from "./Listcomment";
import NewPost from "./newpost";
import EditPost from "./editpost";
import NewComment from "./newcoment";
import RightPost from './rightpost';
import "./post.scss";
import { Button, Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
const ListPost = props => {
  const [isShowing, setIsShowing] = useState(false);
  const [isShowing2, setIsShowing2] = useState(false);
  const [isShowing3, setIsShowing3] = useState(false);
  function toggle() {
    setIsShowing(!isShowing);
  }
  function toggle2() {
    setIsShowing2(!isShowing2);
  }
  function toggle3() {
    setIsShowing3(!isShowing3);
  }
  const posts = props.post.posts;
  const likeandunlike = id => {
    props.likeandunlike(id);
  };
  const deletepost = id => {
    props.deletepost(id);
    toggle2();
  };
  useEffect(() => {
    props.listpost();
  }, [props.listpost]);
  console.log(posts)
  return (
    <div className="row">
      

      <div className="Posts">
      <div className='row'>
      <div className="new">
        <Button color="btn btn-link" onClick={toggle}>
          New <i className="fa fa-plus-square" aria-hidden="true" />
        </Button>
        <NewPost isShowing={isShowing} toggle={toggle} />
      </div>
      </div>
        <div className="row">
          <div className="col-sm-8">
            <div className="leftposts">
              <div className="Post">
                {(posts && posts.length) > 0
                  ? posts.map((post, index) => (
                      <div key={index} className="posts">
                        {(post.User.Image && post.User.Image.length) > 0 ? (
                          <img
                            src={`../upload/user/${
                              post.User.Image[post.User.Image.length - 1]
                            }`}
                            className="postimageIcon"
                          />
                        ) : (
                          <img
                            src={"../upload/noimage/noimage.jpg"}
                            className="postimageIcon"
                          />
                        )}
                        {post.User._id === props.user.user.id ? (
                         
                          <div className="deletepost">
                            <Button
                              color="btn btn-link"
                              onClick={toggle2}
                              // onClick={deletepost.bind(null, post._id)}
                            >
                              Delete{" "}
                              <i className="fa fa-trash" aria-hidden="true" />
                            </Button>
                            {/* Delete */}
                            <Modal isOpen={isShowing2} toggle={toggle2}>
                              <ModalHeader toggle={toggle2}>Delete</ModalHeader>
                              <ModalBody>Are you sure</ModalBody>
                              <ModalFooter>
                                <Button
                                  color="primary"
                                  onClick={deletepost.bind(null, post._id)}
                                >
                                  Delete
                                </Button>{" "}
                                <Button color="secondary" onClick={toggle2}>
                                  Cancel
                                </Button>
                              </ModalFooter>
                            </Modal>
                          </div>
                        
                       
                       

                        ) : (
                          ""
                        )}
                        {post.User._id === props.user.user.id ?    <div className="editpost">
                          <Link to={`/EditPost/${post._id}`}>Edit<i className="far fa-edit" /></Link>
                        </div>:''}
                     
                        <div className="namepost">
                          <p>{post.User.fullName}</p>
                        </div>
                        <div className="time">
                          {<ReactMoment fromNow>{post.Date}</ReactMoment>}
                        </div>
                        <div className="postsDetail">
                          <p>{post.Detail}</p>
                        </div>
                        <div className="postsImage">
                          {(post.Image && post.Image.length) > 0 ? (
                            <img src={`../upload/post/${post.Image[post.Image.length-1]}`} />
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="menu">
                          <button
                            className="button"
                            onClick={likeandunlike.bind(null, post._id)}
                          >
                            <i className="fas fa-thumbs-up" /> Thích (
                            {post.Like.length})
                          </button>
                          <button className="button">
                            <i className="far fa-comment-alt" /> Bình luận (
                            {post.Comment.length})
                          </button>
                          <button className="button">
                            <i className="fas fa-share-alt" /> Share
                          </button>
                        </div>
                        <div className="comment">
                          <ListComment comment={post.Comment} />
                        </div>
                        <div className="newcomment">
                          <NewComment id={post._id} />
                        </div>
                      </div>
                    ))
                  : ""}
              </div>
            </div>
          </div>
          <div className="col-sm-4" >
         <RightPost/>
          </div>
          
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = state => {
  return {
    post: state.post,
    user: state.user
  };
};
export default connect(
  mapStateToProps,
  { listpost, likeandunlike, deletepost }
)(ListPost);
