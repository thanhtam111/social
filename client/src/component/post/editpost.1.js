import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { geteditpost,postedit } from "../../actions/postAction";
import _ from 'lodash';
import './post.scss';

import { Button, Modal, ModalHeader, ModalBody,Form,  Input, FormFeedback } from 'reactstrap';
function Modals(props){
const id=props.match.params.id

const [Detail, setDetail] = useState("");
const [file, setfile] = useState([]);
const [Image, setImage] = useState([]);
const [errors, seterrors] = useState({});

useEffect(() => {
  if (!_.isEmpty(props.post.posts)) {
    setDetail(props.post.posts.Detail);
    setImage(props.post.posts.Image);
  }
  
}, [props.post.posts]);

const onSubmit = e => {
  e.preventDefault();
  const formData = new FormData();
  for (let i = 0; i < file.length; i++) {
    formData.append(Image, file[i]);
  }
  // formData.append("Title", Title);
  formData.append("Detail", Detail);

  props.postedit(id, formData,props.history);
};
useEffect(() => {
  seterrors(props.errors);
}, [errors, props.errors]);
const deleteid = id => {
  if (window.confirm("Are you sure")) {
    const a = [...file].filter(e => e != id);
    setfile(a);
  }
};

useEffect(() => {
  props.geteditpost(id);
}, [props.geteditpost]);

const image=(Image && Image.length)>0? Image.map((img,index)=>(
<img src={`../upload/post/${img}`} key={index} width='50px'></img>
)):''
  const isShowing=props.isShowing
  const toggle=props.toggle
    return (
        <Modal isOpen={isShowing} toggle={toggle}>
          <ModalHeader toggle={toggle}>EDIT POST</ModalHeader>
          <ModalBody>
          <Form onSubmit={onSubmit}>
          <br />
          <div>
            <h4>Detail</h4>
            <Input
              type="textarea"
              name="Detail"
              id="Detail"
              value={Detail}
              onChange={e => setDetail(e.target.value)}
              invalid={errors.Detail ? true : false}
            />
            <FormFeedback>{errors.Detail ? errors.Detail : null}</FormFeedback>
          </div>
          <br />

          <div className="input-group-prepend">
            <h3>Image:</h3>
          </div>
          <div>
            <Input
              type="file"
              name="file"
              id="file"
              multiple
              placeholder="Enter file..."
              onChange={e => setfile(e.target.files)}
              accept="image/*"
            />

            {(file && file.length) > 0
              ? [...file].map((imgs, index) => (
                  <div key={index} className="imageproduct">
                    <img src={URL.createObjectURL(imgs)} />

                    <span onClick={deleteid.bind(this, imgs)}>
                      <i className="fa fa-window-close" />
                    </span>
                  </div>
                ))
              : ""}
          </div>
          <br />
                  <div>
                    {/* {image} */}
                  </div>
                  <div className="button-post">
          <Button color="primary"
          //  onClick={toggle}
           >Save</Button>{' '}
             <Button color="primary"
          onClick={toggle}
           >Cancel</Button>{' '}
          </div>
        </Form>
          </ModalBody>
        
        </Modal>
    )
}

const mapStateToProps = state => {
  return {
    user: state.user,
    errors: state.errors,
    post:state.post
  };
};
export default connect(
  mapStateToProps,
  { geteditpost,postedit }
)(Modals);
