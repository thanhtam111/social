import React, { useState ,useEffect} from 'react';
import {connect} from 'react-redux';
import './user.css';
import { Button, Input, FormFeedback } from 'reactstrap';
import {signin} from '../../actions/userActions';
const SignIN=(props)=>{
const [fullName,setValue]=useState('');
const [email,setEmail]=useState('');
const [password,setPassword]=useState('');
const [file,setfile]=useState('');
const [errors,seterrors]=useState({});
const onSubmit=(e)=>{
    e.preventDefault();
    const formData=new FormData();
    formData.append('fullName',fullName);
    formData.append('email',email);
    formData.append('password',password);
    formData.append('Image',file)
   props.signin(formData,props.history)
}


useEffect(
    () => {
        seterrors(props.errors)
    },
    [errors, props.errors]
  )
return (
    
    <div className='form-signin'>
    <div className='lable'>
  
     <div className='cart-body'>

     <form onSubmit={onSubmit}>
     <div className='lable'>
    <span className='lable'>
      SIGNIN
     </span>
    </div>
   <br/>
     <div className='input-group mb-3'>
        <div className="input-group-prepend">
                        <h3>Full Name:</h3>
                    </div>
                    <div>
                    <Input
                    
                            type="text"
                            name="fullName"
                            id="fullName"
                            placeholder="Enter fullName..."
                            onChange={e => setValue(e.target.value)}
                            invalid={errors.fullName  ? true : false}
                        />
                        
                        <FormFeedback>{errors.fullName  ? errors.fullName  : null}</FormFeedback>         

                       
                    </div>
                </div>
                <div className='input-group mb-3'>
        <div className="input-group-prepend">
        <h3>Email:</h3>
                    </div>
                    <div>
                    <Input
                            type="email"
                            name="email"
                            id="email"
                            placeholder="Enter email..."
                            onChange={e => setEmail(e.target.value)}
                            invalid={errors.email ||errors.noemail ? true : false}
                        />
                        
                        <FormFeedback>{errors.email ||errors.noemail ? errors.email ||errors.noemail : null}</FormFeedback>         
                      
                    </div>
                </div>
                <div className='input-group mb-3'>
        <div className="input-group-prepend">
        <h3>Password:</h3>
                    </div>
                    <div>
                    <Input
                            type="password"
                            name="password"
                            id="password"
                            placeholder="Enter password..."
                            onChange={e => setPassword(e.target.value)}
                            invalid={errors.password ? true : false}
                        />
                        
                        <FormFeedback>{errors.password ? errors.password : null}</FormFeedback>         
                      
                    </div>
                </div>
                <div className='input-group mb-3'>
        <div className="input-group-prepend">
        <h3>Image:</h3>
                    </div>
                    <div>
                    <Input
                            type="file"
                            name="file"
                            id="file"
                            placeholder="Enter file..."
                            onChange={e => setfile((e.target.files[0]))}
                           accept='image/*'
                        />
                    </div>
                </div>
                
        <div className='button-signin'>
        <Button color="info" size="lg">Signin</Button>
        </div> 
        </form>
     </div>
     </div>
     </div>
       
    
)
}
const mapStateToProps = (state) => {
    return {
        user: state.user,
        errors:state.errors
    }
}
export default connect(mapStateToProps,{signin})(SignIN);