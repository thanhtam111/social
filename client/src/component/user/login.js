import React, { useState ,useEffect} from 'react';
import {connect} from 'react-redux';
import './user.css';
import {Link} from 'react-router-dom';
import { Button, Form,  Input, FormFeedback } from 'reactstrap';
import {login} from '../../actions/userActions';
const SignIN=(props)=>{

const [email,setEmail]=useState('');
const [password,setPassword]=useState('');
const [errors,seterrors]=useState({});
const onSubmit=(e)=>{
    e.preventDefault();
    const newData=({
      
        email:email,
        password:password
    })
   props.login(newData,props.history)
}
useEffect(()=>{
    
    if (props.user.isAuthenticated) {
        props.history.push('/Listpost');
      }
    
})
useEffect(() => {
 seterrors(props.errors)
},[errors,props.errors])
return (
    
    <div className='form-login'>
    
   
  <div className='cart-body'>
  <Form onSubmit={onSubmit}>
  <div className='lable'>
    <span className='lable'>
      LOGIN
     </span>
    </div>
    <br/>      
    <div className='input-group mb-3'>
        <div className="input-group-prepend">
                       <h3>Email</h3>
                    </div>
                    <div>
                    <Input 
                    
                   
                            type="text"
                            name="email"
                            id="email"
                            placeholder="Enter email..."
                            onChange={e => setEmail(e.target.value)}
                            invalid={errors.email ||errors.noemail ? true : false}
                        />
                        <FormFeedback>{errors.email||errors.noemail ? errors.email||errors.noemail : null}</FormFeedback>
                    </div>
                    
                   
                    
        </div>
        <br/>
        <div className='input-group mb-3'>
        <div className="input-group-prepend">
                       <h3>Password</h3>
                    </div>
                    <div>
                   <Input
                           type="password"
                           name="password"
                           id="password"
                           placeholder="Enter password..."
                           onChange={e => setPassword(e.target.value)}
                           invalid={errors.password ||errors.nopassword ? true : false}
                       />
                       
                       <FormFeedback>{errors.password||errors.nopassword ? errors.password||errors.nopassword : null}</FormFeedback>         
                      
                   </div>
               </div>
               <div className='input-group mt-3'>
        <Link to='/Signin' className='login-link '>Create an account</Link>
        </div>
        
       
       
        <br/>
        <div className='button-login'>
        <Button color="info" size="lg">Login</Button>
        </div>
       </Form>
  </div>
  </div>

       
    
)
}
const mapStateToProps = (state) => {
    return {
        errors:state.errors,
        user:state.user
    }
}
export default connect(mapStateToProps,{login})(SignIN);