const express=require('express');
const passport=require('passport');
const Profile=require('../../models/profile');
const User=require('../../models/user');

const {getProfile,geteditProfile,deleteProfile}=require('../../controler/getprofilecontroler');
const validatorprofile=require('../../validator/validatorprofile');
const router=express.Router();

////show profile
router.get('/', passport.authenticate('jwt', { session: false }),getProfile);
//add profile
router.post('/',passport.authenticate('jwt', {session: false}),(req,res)=>{
    const {Address,Phone,DOB}=req.body;
    const {errors,isValid}=validatorprofile(req.body);
    if(!isValid){
        return res.status(400).json(errors);
    }
    //check image

Profile.findOne({User:req.user._id}).then(pro=>{
  Profile.findOne({Phone:req.body.Phone}).then(pro=>{
      if(pro){
          return res.status(400).json({nophone:'Phone is used'})
      }
  })
    const newProfile=new Profile({
        User:req.user._id,
        Address,Phone,DOB
    })
    newProfile.save().then(pro=>{
        res.status(200).json(pro)
    })

}).catch(console.log())

})
//get editprofile
router.get('/editprofile/:id',  passport.authenticate('jwt', { session: false }),geteditProfile);
//post editprofile
router.post('/editprofile/:id',  passport.authenticate('jwt', { session: false }),(req,res)=>{
    const {errors,isValid}=validatorprofile(req.body);
    if(!isValid){
        return res.status(400).json(errors);
    }
    const {Address,Phone,DOB}=req.body;
  Profile.findById(req.params.id).then(pro=>{
      pro.Address=Address;
      pro.Phone=Phone;
      pro.DOB=DOB;
   
        pro.save().then(pro=>res.status(200).json(pro))
   
  })
   
})
///delete profile
router.delete(
    '/',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
      Profile.findOneAndRemove({ user: req.user.id }).then(() => {
        User.findOneAndRemove({ _id: req.user.id }).then(() =>
          res.json({ success: true })
        );
      });
    }
  );
module.exports=router
